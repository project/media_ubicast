<?php

namespace Drupal\media_ubicast\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media_ubicast\Plugin\media\Source\Ubicast;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates Ubicast video URLs.
 */
class ValidUbicastUrlConstraintValidator extends ConstraintValidator {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint): void {
    /** @var \Drupal\media\MediaInterface $media */
    $media = $value->getEntity();
    $source = $media->getSource();
    if (!($source instanceof Ubicast)) {
      throw new \LogicException('Media source must implement ' . Ubicast::class);
    }

    $url = $source->getSourceFieldValue($media);
    $url = trim($url, '/');
    // The URL may be NULL if the source field is empty, which is invalid input.
    if (empty($url)) {
      $this->context->addViolation($constraint->emptyUrlMessage);
      return;
    }

    if (!str_contains($url, 'ubicast.tv/videos/')) {
      $this->context->addViolation($constraint->invalidUrlMessage);
    }
  }

}
