<?php

namespace Drupal\media_ubicast\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a value represents a valid Ubicast video URL.
 *
 * @Constraint(
 *   id = "valid_ubicast_url",
 *   label = @Translation("Valid Ubicast URL"),
 *   type = {"string"}
 * )
 */
class ValidUbicastUrlConstraint extends Constraint {

  /**
   * The error message if the URL is empty.
   *
   * @var string
   */
  public $emptyUrlMessage = 'The URL cannot be empty.';

  /**
   * The error message if the URL does not match.
   *
   * @var string
   */
  public $invalidUrlMessage = 'The given URL is not valid. Valid Ubicast video URLs contain "ubicast.tv/videos/".';

}
