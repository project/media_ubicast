<?php

namespace Drupal\media_ubicast\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;

/**
 * A media source plugin for Ubicast videos.
 *
 * @MediaSource(
 *   id = "media_ubicast",
 *   label = @Translation("Ubicast Video"),
 *   description = @Translation("A media source plugin for Ubicast videos."),
 *   allowed_field_types = {"string"},
 *   default_thumbnail_filename = "video.png"
 * )
 */
class Ubicast extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * Key for the "Name" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_NAME = 'name';

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      static::METADATA_ATTRIBUTE_NAME => $this->t('Name'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $url = $media->get($this->configuration['source_field'])->value;
    // If the source field is not required, it may be empty.
    if (empty($url)) {
      return parent::getMetadata($media, $attribute_name);
    }
    switch ($attribute_name) {
      case static::METADATA_ATTRIBUTE_NAME:
      case 'default_name':
        // We cannot use Drupal\Component\Utility\UrlHelper::parse() to parse
        // the URL because it includes the scheme and host in the returned path.
        $path = parse_url($url, PHP_URL_PATH);
        return ucfirst(str_replace(['/videos/', '/', '-'], ['', ' ', ' '], trim($path, '/')));

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [
      'valid_ubicast_url' => [],
    ];
  }

}
